export default (uri, queryParameters) => {
  let buffer = uri;

  Object.entries(queryParameters).forEach(([key, value]) => {
    const re = new RegExp(`([?&])${key}=.*?(&|$)`, "i");
    const separator = buffer.indexOf("?") !== -1 ? "&" : "?";
    if (uri.match(re)) {
      buffer.replace(re, `$1${key}=${value}$2`);
    }
    buffer += `${separator + key}=${value}`;
  });

  return buffer;
};
