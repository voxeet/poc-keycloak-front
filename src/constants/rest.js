// api endpoint
export const API_ENDPOINT = "http://10.139.17.16:8080/v2";

// resources
export const RESOURCES = {
  ME: "/users/me"
};

// methods
export const METHODS = {
  POST: "post",
  GET: "get",
  PUT: "put",
  DELETE: "delete"
};

// application type
export const APPLICATION_TYPE = {
  JSON: "application/json",
  OCTET_STREAM: "application/octet-stream",
  FORM_DATA: "multipart/form-data"
};
