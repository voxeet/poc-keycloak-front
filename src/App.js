import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { API_ENDPOINT, RESOURCES, METHODS } from "./constants/rest";
import { callApi } from "./helpers/api";

import {
  useAuthentication,
  AuthenticationProvider
} from "./AuthenticationProvider";

const Home = () => <div>home</div>;

const Protected = () => {
  const {
    isAuthenticated,
    logout,
    user,
    authenticationClient
  } = useAuthentication();

  useEffect(() => {
    if (authenticationClient) {
      const token = authenticationClient.token;
      const request = {
        endpoint: {
          url: `${API_ENDPOINT + RESOURCES.ME}`,
          httpVerb: METHODS.GET
        },
        token
      };
      callApi(request).then(data => console.log(data));
    }
  }, []);

  return isAuthenticated ? (
    <div>
      <div>{user.username}</div>
      <button onClick={() => logout({ redirectUri: "http://localhost:3000" })}>
        logout
      </button>
    </div>
  ) : (
    <div>protected</div>
  );
};

const Main = () => {
  const { loading } = useAuthentication;

  return loading ? (
    <div>loading...</div>
  ) : (
    <Router>
      <ul>
        <li>
          <Link to="/">public component</Link>
        </li>
        <li>
          <Link to="/protected">protected component</Link>
        </li>
      </ul>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/protected" component={Protected} />
      </Switch>
    </Router>
  );
};

function App() {
  return (
    <AuthenticationProvider>
      <Main />
    </AuthenticationProvider>
  );
}

export default App;
