import React, { useEffect, useState, useContext } from "react";
import Keycloak from "keycloak-js";
import { fromUnixTime } from "date-fns";

export const AuthenticationContext = React.createContext();
export const useAuthentication = () => useContext(AuthenticationContext);

export const AuthenticationProvider = ({ children, ...initOptions }) => {
  const [isAuthenticated, setIsAuthenticated] = useState();
  const [user, setUser] = useState();
  const [authenticationClient, setAuthenticationClient] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const initAuthentication = async () => {
      const keycloak = Keycloak("/keycloak.json");
      keycloak.onTokenExpired = () => {
        console.log("token expired");
        keycloak.logout();
      };
      keycloak.onAuthError = () => console.log("access denied");
      keycloak.onAuthSuccess = () => console.log("access granted");
      const authenticated = await keycloak.init({
        promiseType: "native",
        onLoad: "login-required"
      });

      if (authenticated) {
        const { exp } = keycloak.tokenParsed;
        console.log(fromUnixTime(exp));
        const userProfile = await keycloak.loadUserProfile();
        setUser(userProfile);
        setIsAuthenticated(true);
        setAuthenticationClient(keycloak);
      }
      setLoading(false);
    };
    initAuthentication();
  }, []);

  return (
    <AuthenticationContext.Provider
      value={{
        isAuthenticated,
        user,
        loading,
        authenticationClient,
        logout: (...p) => authenticationClient.logout(...p)
      }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
};
